#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 14:44:09 2022

@author: ggleizer
"""

import ETCetera.Systems.linearetc as etc
import ETCetera.Abstractions as ab
import numpy as np
import time
import scipy.linalg as la


Ap = np.array([[0, 1], [-2, 3]])
Bp = np.array([0, 1])
p = etc.LinearPlant(Ap, Bp, np.eye(2))

K = np.array([[0, -5]])
h = 0.05

sigmas = [0.1, 0.2, 0.3, 0.4, 0.5]
traffics = {}
for sigma in sigmas:
    controller = etc.LinearController(K, h)
    trig = etc.TabuadaPETC(p, controller, sigma=sigma,
                                  h=h, kmax=20)

    t = time.time()
    traffic = ab.TrafficModelLinearPETC(trig,
                                        depth=50,
                                        etc_only=True,
                                        solver='z3',
                                        symbolic=False,
                                        stop_if_mace_equivalent=True)
    traffic.create_abstraction()
    traffic.elapsed_time_s = time.time() - t

    traffics[sigma] = traffic


'''Begin presenting results'''


print('\n\n========================RESULTS========================\n')
print("{:<8} {:<8} {:<15} {:<15}".format(
    'Sigma','l','SAIST','CPU Time [s]'))
for s, t in traffics.items():
    l = max(len(x) for x in t.regions)
    saist, cpu_time = t.limavg, t.elapsed_time_s
    print ("{:<8} {:<8} {:<15.4f} {:<10.0f}".format(s, l, saist, cpu_time))

''' Additional results presented in the paper '''
t1 = traffics[0.1]
print('\nEigenvalues for sigma=0.1:')
for i,m in t1.M.items():
    print(f'\tM({i}): {la.eigvals(m)}')

# Maximum average cycle for t1
val, cycle_upper = t1.automaton.minimum_average_upper_bound()
print(f'\nUpper Bound on SAIST (Prop. 4) for sigma=0.1: {val}')

# Display all behavioral cycles
print('\n Cycles:')
for s, t in traffics.items():
    if len(t.cycles) > 0:
        cycle = next(c for c in t.cycles)
        print(f'\tSigma={s}: {cycle} (length={len(cycle)})')
