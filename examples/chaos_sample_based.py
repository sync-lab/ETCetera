#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 18 17:01:07 2020
Moved to ETCetera on Thu May 19 17:00:02 2022

@author: ggleizer
"""

import ETCetera.Systems.linearetc as etc
import ETCetera.Abstractions as ab
import ETCetera.util.etcsim as etcsim

import matplotlib.pyplot as plt
import numpy as np
from numpy import random
from scipy import stats
import pickle


def _cdf_cvm_inf(x):
    """
    Calculate the cdf of the Cramér-von Mises statistic (infinite sample size).
    See equation 1.2 in Csorgo, S. and Faraway, J. (1996).
    Implementation based on MAPLE code of Julian Faraway and R code of the
    function pCvM in the package goftest (v1.1.1), permission granted
    by Adrian Baddeley. Main difference in the implementation: the code
    here keeps adding terms of the series until the terms are small enough.
    The function is not expected to be accurate for large values of x, say
    x > 4, when the cdf is very close to 1.

    FIXME: From Scipy 1.7.0, which I cannot upgrade now
    """
    from scipy.special import gammaln, kv

    x = np.asarray(x)

    def term(x, k):
        # this expression can be found in [2], second line of (1.3)
        u = np.exp(gammaln(k + 0.5) - gammaln(k+1)) / (np.pi**1.5 * np.sqrt(x))
        y = 4*k + 1
        q = y**2 / (16*x)
        b = kv(0.25, q)
        return u * np.sqrt(y) * np.exp(-q) * b

    tot = np.zeros_like(x, dtype='float')
    cond = np.ones_like(x, dtype='bool')
    k = 0
    while np.any(cond):
        z = term(x[cond], k)
        tot[cond] = tot[cond] + z
        cond[cond] = np.abs(z) >= 1e-7
        k += 1

    return tot


def cramervonmises_2samp(x, y):
    """Perform the two-sample Cramér-von Mises test for goodness of fit.

    FIXME: From Scipy 1.7.0, which I cannot upgrade now
    """
    xa = np.sort(np.asarray(x))
    ya = np.sort(np.asarray(y))

    if xa.size <= 1 or ya.size <= 1:
        raise ValueError('x and y must contain at least two observations.')
    if xa.ndim > 1 or ya.ndim > 1:
        raise ValueError('The samples must be one-dimensional.')

    nx = len(xa)
    ny = len(ya)

    # get ranks of x and y in the pooled sample
    z = np.concatenate([xa, ya])
    # in case of ties, use midrank (see [1])
    r = stats.rankdata(z, method='average')
    rx = r[:nx]
    ry = r[nx:]

    # compute U (eq. 10 in [2])
    u = nx * np.sum((rx - np.arange(1, nx+1))**2)
    u += ny * np.sum((ry - np.arange(1, ny+1))**2)

    # compute T (eq. 9 in [2])
    k, N = nx*ny, nx + ny
    t = u / (k*N) - (4*k - 1)/(6*N)

    # method = asymptotic
    # compute expected value and variance of T (eq. 11 and 14 in [2])
    et = (1 + 1/N)/6
    vt = (N+1) * (4*k*N - 3*(nx**2 + ny**2) - 2*k)
    vt = vt / (45 * N**2 * 4 * k)

    # computed the normalized statistic (eq. 15 in [2])
    tn = 1/6 + (t - et) / np.sqrt(45 * vt)

    # approximate distribution of tn with limiting distribution
    # of the one-sample test statistic
    # if tn < 0.003, the _cdf_cvm_inf(tn) < 1.28*1e-18, return 1.0 directly
    if tn < 0.003:
        p = 1.0
    else:
        p = max(0, 1. - _cdf_cvm_inf(tn))

    return t, p


if __name__ == '__main__':

    try:
        with open('traffic_tac.pickle', 'rb') as pik:
            cases, traffics, rtraffics = pickle.load(pik)
    except FileNotFoundError:
        raise FileNotFoundError('Run examples/chaos_abstractions.py first'
                                ' to save the abstractions')

    rtraffic = rtraffics[2]
    trig = rtraffic.trigger
    h = rtraffic.h

    comp, size, attr = rtraffic.automaton.generate_components()
    chaotic = (size > 1)



    ''' ENSEMBLE '''

    def next_sample(th):
        x0 = np.array([np.cos(th), np.sin(th)])
        out = etcsim.simulate_sample_and_hold_control(trig, np.arange(0,2,h),
                                                      x0)
        xs = out['xphat'][:,out['sample']]
        thout = np.arctan(xs[1]/xs[0])

        dt = np.diff(out['t'][out['sample']])

        return thout, dt

    def build_ensemble(base='uniform', mean=0.0):
        F = []
        Y = []
        iii = 0
        while len(F) < 20:  # This loop is very slow, few points are in fact in
                            # the chaotic invariant set
            if base == 'uniform':
                th = random.rand()*np.pi
            else:
                th = random.randn() + mean
                while th < 0:
                    th += 2*np.pi
            x = np.array([np.cos(th), np.sin(th)])
            r = rtraffic.region_of_state(x)
            idr = rtraffic.automaton.regions.index(r)
            if chaotic[comp[idr]]:
                F.append(th)
                Y.append(r[0])
            iii += 1

        # This should give a good estimate of the invariant set
        Fl, Fu, mean = min(F), max(F), np.mean(F)

        while len(F) < 1000:
            if base == 'uniform':
                th = random.rand()*(Fu - Fl) + Fl
            else:
                th = random.randn()*(Fu - Fl)/4+ mean
            x = np.array([np.cos(th), np.sin(th)])
            r = rtraffic.region_of_state(x)
            idr = rtraffic.automaton.regions.index(r)
            if chaotic[comp[idr]]:
                F.append(th)
                Y.append(r[0])
            iii += 1

        return np.array(F), np.array(Y, dtype='int64')

    # Build initial ensemble
    F, Y = build_ensemble()

    # Build second ensemble
    G, W = build_ensemble(base='normal', mean=np.mean(F))


    Fnext = F.copy()
    Ynext = Y.copy()
    Gnext = G.copy()
    Wnext = W.copy()
    its = 0
    while True:
        F[:] = Fnext[:]
        Y[:] = Ynext[:]
        for i, t in enumerate(F):
            tout, dt = next_sample(t)
            Fnext[i] = tout[1]
            Ynext[i] = round(dt[0]/h)

        G[:] = Gnext[:]
        W[:] = Wnext[:]
        for i, t in enumerate(G):
            tout, dt = next_sample(t)
            Gnext[i] = tout[1]
            Wnext[i] = round(dt[0]/h)

        cvm, p = cramervonmises_2samp(Ynext, Wnext)
        print(f'p-value: {p}')

        if p > 0.95:
            break

        its += 1

    print(f'Prob. that two ensembles come from same process: {p}')

    print(stats.describe(Ynext))
    plt.figure()
    plt.hist(Ynext, bins=sorted(list(set(Ynext))))
    plt.show()

    print(f'Ensamble average: {np.mean(Ynext)*h}')

    # Now check a specific run with 1000 samples
    tsr = np.zeros(G.shape)
    t = G[235]
    for i in range(G.shape[0]):
        tout, dt = next_sample(t)
        tsr[i] = dt[0]
        t = tout[1]

    plt.figure()
    plt.plot(tsr)
    plt.plot(np.cumsum(tsr)/np.arange(1,1001))
    plt.show()

    print(f'Time-average of a run: {np.mean(tsr)}')



