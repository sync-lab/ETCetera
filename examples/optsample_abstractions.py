#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  3 12:03:59 2022

@author: ggleizer
"""

import numpy as np

import ETCetera.Systems as systems
import ETCetera.Abstractions as abstr

# import logging

# logging.getLogger().setLevel(logging.INFO)

# Define LTI system matrices
A = np.array([[0, 1], [-2, 3]])
B = np.array([[0],[1]])
K = np.array([[1, -4]])
Pl = np.array([[1, .25], [.25, 1]])
Acl = A + B @ K
Ql = -(Acl.T @ Pl + Pl @ Acl)

# PETC parameters
h = 0.1
kmax = 20
rho = 0.8

# Construct object representing the PETC system
plant = systems.LinearPlant(A, B)
controller = systems.LinearController(K, h)
trigger = systems.DirectLyapunovPETC(plant, controller, P=Pl, Q=Ql, rho=rho,
                              h=h, kmax=kmax, predictive=True)

''' Create Traffic Models '''

''' First, verify the performance of the nominal PETC system '''

# Best selection for verification:
#    solver='z3', etc_only, stop_if_mace_equivalent, smart_mace
traffic = abstr.TrafficModelLinearPETC(trigger,
                                   #symbolic=True,
                                   depth=100,
                                   etc_only=True,
                                   solver='z3',
                                   stop_if_mace_equivalent=True,
                                   smart_mace=True)

regions, transitions = traffic.create_abstraction()

print(f'At depth {traffic.final_depth}, a SACE-simulation was obtained with '
      f'SAIST = traffic.limavg * h')
print(f'Smallest average cycles: {traffic.cycles}')
traffic.export('optsample_baseline', 'pickle')


''' Now compute optimized strategies '''

traffic_synth = abstr.TrafficModelLinearPETC(trigger,
                                         depth=1,
                                         etc_only=False,  # <--
                                         solver='z3',
                                         early_trigger_only=True)  # <--
for l in [1, 2, 3]:
    # Print obtained SAIST and its minimizing cycle
    if l > 1:
        # Clear strategies
        traffic_synth.strat = None
        traffic_synth.strat_transition = None
        # Refine model
        traffic_synth.refine()

    # Solve mean-payoff game to find new strategy
    strat, strat_tran = traffic_synth.optimize_sampling_strategy()
    traffic_synth.export(f'optsample_l{l}', 'pickle')

    # Compute its SAIST value (note the last two arguments)
    traffic2 = abstr.TrafficModelLinearPETC(trigger,
                                        #symbolic=True,
                                        depth=100,
                                        etc_only=True,
                                        solver='z3',
                                        stop_if_mace_equivalent=True,
                                        smart_mace=True,
                                        strategy=strat,  # <--
                                        strategy_transition=strat_tran)  # <--

    regions2, transitions2 = traffic2.create_abstraction()
    traffic2.export(f'optsample_saist_l{l}', 'pickle')

    print(f'Optimized SAIST for l={l} is {traffic2.limavg * h}')

