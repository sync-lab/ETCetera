#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 14:44:09 2022

@author: ggleizer
"""

import ETCetera.Systems.linearetc as etc
import ETCetera.Abstractions as ab
import numpy as np
import time
import scipy.linalg as la
import pickle

SAVED = False  # Switch to True if you have already run this code

Ap = np.array([[0, 1, 0], [0, 0, 1], [1, -1, -1]]);
Bp = np.array([0, 0, 1]);
Cp = np.eye(3);
p = etc.LinearPlant(Ap, Bp, Cp)

K = np.array([[-2, -1, -1]])
h = 0.1

sigmas = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

if SAVED:
    with open('traffics_nahs_3d.pickle', 'rb') as f:
        traffics = pickle.load(f)
else:
    traffics = {}
    for sigma in sigmas:
        controller = etc.LinearController(K, h)
        trig = etc.TabuadaPETC(p, controller, sigma=sigma,
                                      h=h, kmax=20)

        t = time.time()
        traffic = ab.TrafficModelLinearPETC(trig,
                                            depth=200,
                                            etc_only=True,
                                            solver='z3',
                                            symbolic=False,
                                            smart_mace=True,  # Uses Alg. 3
                                            stop_if_mace_equivalent=True)
        traffic.create_abstraction()
        traffic.elapsed_time_s = time.time() - t

        traffics[sigma] = traffic

        # Save because it takes long
        with open('traffics_nahs_3d.pickle', 'wb') as f:
            pickle.dump(traffics, f)


'''Begin presenting results'''


print('\n\n========================RESULTS========================\n')
print("{:<8} {:<8} {:<15} {:<15} {:<8}".format(
    'Sigma','l','SAIST','CPU Time [s]', 'SACE Simulation'))
for s, t in traffics.items():
    l = max(len(x) for x in t.regions)
    sace = len(t.cycles) > 0
    saist, cpu_time = t.limavg, t.elapsed_time_s
    print ("{:<8} {:<8} {:<15.4f} {:<15.0f} {!s:<8}".format(s, l, saist, cpu_time, sace))
