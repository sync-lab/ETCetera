#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 15:47:56 2022

@author: ggleizer
"""

import logging
import numpy as np
import scipy.linalg as la
import time

import ETCetera.Systems.linearetc as etc
import ETCetera.Abstractions as ab

Ap = np.array([[0, 1], [-2, 3]])
Bp = np.array([0, 1])
p = etc.LinearPlant(Ap, Bp, np.eye(2))
K = np.array([[1, -4]])

# Lyapunov matrices

Pl = np.array([[1, .25], [.25, 1]])
Acl = p.A + p.B @ K
Ql = -(Acl.T @ Pl + Pl @ Acl)

# Parameters used in the paper.
h = 0.1
hstar = 0.4
r = 0.1
rho = 0.8
# controller and triggering mechanism
controller = etc.LinearController(K, h)
trig = etc.DirectLyapunovPETC(p, controller, P=Pl, Q=Ql, rho=rho,
                              h=0.1, kmax=6, predictive=True)

''' Compute some parameters for the paper '''
# Check exponential decay rate
bHeemels, Ps = trig.check_stability_pwa()

'''Find the largest period such that the Lyaopunov function decreases
over samples'''
# (MPETC of the CDC 2020 paper)
dt = 0.001
for t in np.arange(0, 1, dt):
    pc = etc.PeriodicLinearController(p, controller, t)
    Phi = pc.Phi
    dP = Phi.T @ Pl @ Phi - Pl
    if any(np.real(la.eigvals(dP)) > 0):
        break

hstar = t - dt
print(f'Best period satifying invariance assumption: {hstar}')
hstar = np.floor(hstar*100)/100

# lv = logging.INFO
# logger = logging.getLogger()
# logger.setLevel(lv)

trig.controller.h = trig.h

a = trig.check_stability_kmax(trig.P)
print(f'Ratio of Lyapunov contraction at every sample: a = {a}.')

N = np.ceil(np.log(r)/np.log(a))
KM = trig.kmax
num_possibilities = KM*((KM-1)**N-1)/(KM-1)
print(f'Max length to reach rV is N = {N}')
print(f'Upper bound on number of MPETC traces is {num_possibilities}')

print('\n===== BUILDING ABSTRACTION ======\n')

t = time.time()

traffic = ab.TrafficModelLinearPETC(trig,
                                    depth=30,
                                    etc_only=True,
                                    early_trigger_only=True,
                                    end_level=r,
                                    solver='z3',
                                    stop_around_origin=True)  # key flag for MPETC
traffic.create_abstraction()
print('Elapsed: %.2f seconds' % (time.time() - t))

X_MPETC = traffic._all_regions
X_PETC = traffic._marginal

print(f'|X_MPETC| = {len(X_MPETC)}')
print(f'|X_PETC| = {len(X_PETC)}')

Tstar = max(sum(s) for s in X_MPETC) * trig.h
print(f'Time to reach rX: {Tstar}')

b = -np.log(r)/2/Tstar
print(f'GES decay rate bound: b* = {b}; (compare PWA approach in '
      f'Heemels et al (2013), b={bHeemels}).')


min_period = min(np.mean(s) for s in X_PETC)
freq = 1/min_period/trig.h
regs = {s for s in X_PETC if np.mean(s) == min_period}
print(f'Worst-case PETC sampling frequency is larger than {freq} '
      f'(witness regions: {regs}).')



