#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 17:14:11 2022

@author: ggleizer
"""

import ETCetera.util.etcsim as etcsim
import ETCetera.Abstractions as abstr
from ETCetera.Systems.linearssc import SSC

import numpy as np
import matplotlib.pyplot as plt




TTT = 20

l = 2  # Choose l = 2 for the results of the paper/thesis



def generate_plot_data(x0, trig):
    # r = 0.1  # Paper: 0.1
    # controller.h = 0.1
    t0, t1 = 0, TTT
    t = np.arange(t0, t1, 0.01)
    Pl = trig.P

    w = None  # lambda t: np.array([0*np.sin(t)])
    out = etcsim.simulate_sample_and_hold_control(trig, t, x0, [],
                                                  disturbance=w)
    ks = np.array([x for x in range(len(t))])
    ks = ks[out['sample']]
    # dk = np.diff(ks)
    # print(dk/10)
    xs = out['xp']
    xs = xs[:,out['sample']]

    # Check Lyap
    Vall = [x.T @ Pl @ x for x in out['xp'].T]
    Vtrig = [x.T @ Pl @ x for x in xs.T]

    # plt.plot(np.arange(t0, 5, 0.01), xall.T)
    ttrig = t[ks]

    return xs, out['t'], Vall, Vtrig, ttrig

if __name__ == '__main__':

    try:
        tbaseline = abstr.TrafficModelLinearPETC.from_bytestream_file('optsample_baseline.pickle')
        h = tbaseline.h
        print(f'At depth {tbaseline.final_depth}, a SACE-simulation was obtained with '
              f'SAIST = {tbaseline.limavg * h}')
        print(f'Smallest average cycles: {tbaseline.cycles}')
    except FileNotFoundError:
        raise FileNotFoundError('Run examples/optsample_abstractions.py first')

    try:
        traffic = abstr.TrafficModelLinearPETC.from_bytestream_file(f'optsample_l{l}.pickle')
        tsaist = abstr.TrafficModelLinearPETC.from_bytestream_file(f'optsample_saist_l{l}.pickle')
        print(f'Optimized SAIST for l={l} is {tsaist.limavg * h}')

        # Compute V_U:
        print('Computing V_U (upper bound for SAIST)')
        vu, vu_cycle = tsaist.automaton.minimum_average_upper_bound()
        print(f'V_U = {vu * h}')
    except FileNotFoundError:
        raise FileNotFoundError('Run examples/optsample_abstractions.py first;'
                                 f' make sure to run until the case with l={l}'
                                 ' is concluded.')

    #traffic.strat = None
    trig = traffic.trigger

    Pl = trig.P
    V = lambda x: x.T @ Pl @ x  # Lyapunov function

    #traffic.to_numpy()
    h = trig.h
    strat = traffic.strat.copy()
    traffic.strat = None

    ssc = SSC(traffic, {}, strat)

    '''Example of the PETC'''
    plt.figure(1)
    plt.figure(2)
    # plt.figure(3)
    # plt.figure(4)
    plt.figure(5)
    plt.figure(6)

    np.random.seed(1909)
    for i, x in enumerate(np.random.rand(10,2)):
        x = x - 0.5
        x0 = x/np.sqrt(V(x))

        xs, tall, Vall, Vtrig, ttrig = generate_plot_data(x0, trig)

        plt.figure(1)
        plt.plot(tall, Vall, 'orange')
        plt.plot(ttrig, Vtrig, 'gx')

        # plt.figure(3)
        dt = np.diff(ttrig)
        nn = len(dt)
        runavg = np.cumsum(dt)/(np.arange(nn)+1)
        # plt.plot(ttrig[:-1], dt, 'gx')
        # plt.plot(ttrig[:-1], runavg, 'orange')

        xss, talls, Valls, Vtrigs, ttrigs = generate_plot_data(x0, ssc)

        plt.figure(2)
        plt.plot(talls, Valls, 'orange')
        plt.plot(ttrigs, Vtrigs, 'gx')

        # plt.figure(4)
        dts = np.diff(ttrigs)
        nns = len(dts)
        runavgs = np.cumsum(dts)/(np.arange(nns)+1)
        # plt.plot(ttrigs[:-1], dts, 'gx')
        # plt.plot(ttrigs[:-1], runavgs, 'orange')

        plt.figure(5)
        plt.plot(ttrig[:-1], runavg, 'g')
        plt.plot(ttrigs[:-1], runavgs, 'orange')

        if i == 0:
            plt.figure(6)
            deadlines = [h*traffic.region_of_state(x)[0] for x in xss.T]
            plt.stem(ttrigs[:-1], deadlines[:-1], linefmt='red', markerfmt='r_',
                     basefmt='None')
            plt.stem(ttrigs[:-1], dts, linefmt='orange', markerfmt=('orange','o'),
                     basefmt='None')
            plt.stem(ttrig[:-1], dt, linefmt='green', markerfmt=('green', 'x'),
                     basefmt='None')
            plt.legend(('Deadline', f'Our SDSS ($l={l}$)', 'PETC'))


    plt.figure(1)
    plt.title('Evolution of Lyapunov function for ETC (10 initial conditions)')
    plt.xlabel('t')
    plt.ylabel('V')
    plt.legend('','Samples')

    plt.figure(2)
    plt.title(f'Evolution of Lyapunov function for SDSS (l={l}) (10 initial conditions)')
    plt.xlabel('t')
    plt.ylabel('V')
    plt.legend('','Samples')

    # plt.figure(3)
    # plt.plot([0, TTT], [7/30, 7/30], 'gray')

    # plt.figure(4)
    # plt.plot([0, TTT], [.58, .58], 'gray')

    plt.figure(5)
    plt.xlabel('$t$')
    plt.ylabel('IST running average')
    plt.legend(('PETC', f'SDSS with $l={l}$'))
    plt.grid()