#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 22:52:34 2022

@author: ggleizer
"""

import ETCetera.Systems.linearetc as etc
import ETCetera.util.etcsim as etcsim
from ETCetera.Abstractions.LinearPETC.utils.optim import Ellipse

import matplotlib.pyplot as plt
import numpy as np



def generate_plot_data(x0):
    # r = 0.1  # Paper: 0.1
    # controller.h = 0.1
    t0, t1 = 0, 5
    t = np.arange(t0, t1, 0.01)

    w = None  # lambda t: np.array([0*np.sin(t)])
    out = etcsim.simulate_sample_and_hold_control(trig, t, x0, [], disturbance=w)
    ks = np.array([x for x in range(len(t))])
    ks = ks[out['sample']]
    dk = np.diff(ks)
    print(dk/10)
    xs = out['xp']
    xs = xs[:,out['sample']]

    # Check Lyap
    V = [x.T @ Pl @ x for x in xs.T]
    for i,v in enumerate(V):
        if v < r:
            break
    kf = ks[i]
    x0p = xs[:,i]
    t0p = t[kf]
    tp = np.arange(t0p, 5, 0.01) - t0p
    peri = etc.PeriodicLinearController(p, controller, hstar)
    outp = etcsim.simulate_sample_and_hold_control(peri, tp, x0p, [],
                                                disturbance=w)
    xall = np.concatenate((out['xp'][:,:kf], outp['xp']), axis=1)
    ksp = np.array([x for x in range(len(tp))]) + kf
    ksp = ksp[outp['sample']]
    kstotal = np.concatenate((ks[:i], ksp))
    xsp = outp['xp'][:,outp['sample']]

    # plt.plot(np.arange(t0, 5, 0.01), xall.T)
    Vall = np.array([x.T @ Pl @ x for x in xall.T])
    tall = np.arange(t0, 5, 0.01)
    ttrig = t[ks[:i+1]]
    Vtrig = [x.T @ Pl @ x for x in xs[:,:i+1].T]
    tperi = t[ksp][1:]
    Vperi = [x.T @ Pl @ x for x in xsp[:,1:].T]

    controller.h = h

    return xs[:,:i+1], tall, Vall, ttrig, Vtrig, tperi, Vperi


if __name__ == '__main__':

    Ap = np.array([[0, 1], [-2, 3]])
    Bp = np.array([0, 1])
    p = etc.LinearPlant(Ap, Bp, np.eye(2))
    K = np.array([[1, -4]])

    # Lyapunov matrices

    Pl = np.array([[1, .25], [.25, 1]])
    Acl = p.A + p.B @ K
    Ql = -(Acl.T @ Pl + Pl @ Acl)

    # Parameters used in the paper.
    h = 0.1
    hstar = 0.4
    r = 0.1
    rho = 0.8
    # controller and triggering mechanism
    controller = etc.LinearController(K, h)
    trig = etc.DirectLyapunovPETC(p, controller, P=Pl, Q=Ql, rho=rho,
                                  h=0.1, kmax=6, predictive=True)

    V = lambda x: x.T @ Pl @ x  # Lyapunov

    '''Plot the ellipsoid thing'''
    plt.figure()
    E1 = Ellipse(Pl)
    E2 = Ellipse(Pl/r)
    z1 = E1.generate_plot_data()
    plt.plot(z1[0,:], z1[1,:])
    z2 = E2.generate_plot_data()
    plt.fill(z2[0,:], z2[1,:])

    x0 = np.array([0.5, 0.75])
    xs = generate_plot_data(x0)[0]
    plt.plot(xs[0,:], xs[1,:], 'x-')

    plt.figure()
    plt.plot(z1[0,:], z1[1,:])
    plt.fill(z2[0,:], z2[1,:])

    # The next sample
    x0 = xs[:,1]
    x0 = x0/np.sqrt(V(x0))
    xs = generate_plot_data(x0)[0]
    plt.plot(xs[0,:], xs[1,:], 'x-')

    '''Example of the MPETC'''
    plt.figure()

    np.random.seed(1909)
    for x in np.random.rand(10,2):
        x = x - 0.5
        x0 = x/np.sqrt(V(x))

        xs, tall, Vall, ttrig, Vtrig, tperi, Vperi = generate_plot_data(x0)

        plt.plot(tall, Vall, 'orange')
        plt.plot(ttrig, Vtrig, 'gx')
        plt.plot(tperi, Vperi, 'bo', fillstyle='none')
        # plt.gca().set_yscale('log')

    plt.xlabel('$t$')
    plt.ylabel('\$V(\\xiv(t))\$')
    plt.legend(('V', 'PETC', 'Periodic'))
