#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 13:57:39 2020
Moved to ECTetera on Thu May 19 14:14:38 2022

@author: ggleizer
"""


import ETCetera.Systems.linearetc as etc
import ETCetera.Abstractions as ab

import numpy as np
import time, pickle
import matplotlib.pyplot as plt

def make_abstractions(h):
    Ap = np.array([[0, 1], [-2, 3]])
    Bp = np.array([0, 1])
    p = etc.LinearPlant(Ap, Bp, np.eye(2))

            # sigma, K, depth
    cases = [(0.2, np.array([[0,-5]]), 50),
             (0.2, np.array([[0,-6]]), 50),
             (0.32, np.array([[0,-6]]), 10)]

    #cases = cases[:3]

    traffics = []
    rtraffics = []

    for sigma, K, depth in cases:
        # controller and triggering mechanism
        controller = etc.LinearController(K, h)
        trig = etc.TabuadaPETC(p, controller, sigma=sigma, h=h, kmax=20)

        t = time.time()
        traffic = ab.TrafficModelLinearPETC(trig,
                                            depth=depth,
                                            etc_only=True,
                                            solver='z3',
                                            stop_if_mace_equivalent=True,
                                            )
        traffic.create_abstraction()
        traffic.elapsed_time = time.time() - t

        t = time.time()
        rtraffic = ab.TrafficModelLinearPETC(trig,
                                             depth=depth,
                                             etc_only=True,
                                             solver='z3',
                                             stop_if_robust_mace_equivalent=True,
                                             )
        rtraffic.create_abstraction()
        rtraffic.elapsed_time = time.time() - t

        traffics.append(traffic)
        rtraffics.append(rtraffic)

    return cases, traffics, rtraffics

if __name__ == '__main__':

    h = 0.05

    try:
        with open('traffic_tac.pickle', 'rb') as pik:
            cases, traffics, rtraffics = pickle.load(pik)
    except FileNotFoundError:
        cases, traffics, rtraffics = make_abstractions(h)
        with open('traffic_tac.pickle', 'wb') as pik:
            pickle.dump((cases, traffics, rtraffics), pik)

    print('\n********************************\n\n')
    for i, (sigma, K, _) in enumerate(cases):
        print(f'sigma={sigma}, K={K}:')
        print(f'\tILA: {traffics[i].limavg * h}')
        print(f'\tRobILA: {rtraffics[i].limavg * h}')
        print(f'\tCycles: {traffics[i].cycles}')
        print(f'\tCycles (rob): {rtraffics[i].cycles}')
        print(f'\tStable Cycles: {rtraffics[i].stable_cycles}')
        print(f'\tAbsolutely unstable Cycles: {rtraffics[i].au_cycles}')
        print(f'\tDepth: {traffics[i].final_depth}')
        print(f'\tDepth (rob): {rtraffics[i].final_depth}')
        print(f'\tComputation time: {traffics[i].elapsed_time}')
        print(f'\tComputation time (rob): {rtraffics[i].elapsed_time}')
        print('\n')


    ''' Get bisimulation for case 1. '''
    sigma, K, depth = cases[0]
    controller = etc.LinearController(K, h)
    p = traffics[0].trigger.plant
    trig = etc.TabuadaPETC(p, controller, sigma=sigma, h=h, kmax=20)

    t = time.time()
    traffic = ab.TrafficModelLinearPETC(trig,
                                        depth=depth,
                                        etc_only=True,
                                        solver='z3',
                                        )
    traffic.create_abstraction()
    traffic.elapsed_time = time.time() - t

    print(f'Bisimulation for case 1 obtained at depth {traffic.final_depth}')


    ''' Plot entropy evolution '''
    plt.figure()
    plt.plot(range(1,rtraffics[0].final_depth+1), rtraffics[0].entropy, 'r',
             range(1,rtraffics[1].final_depth+1), rtraffics[1].entropy, 'b',
             range(1,rtraffics[2].final_depth+1), rtraffics[2].entropy, 'g')
    plt.legend(('Case 1', 'Case 2', 'Case 3'))
    plt.grid()
    plt.xlabel(r'$l$')
    plt.ylabel(r'$h(\mathcal{S})$')
    plt.show()
    try:
        import tikzplotlib as tikz
        tikz.save('tac_entropies.tex')
    except ModuleNotFoundError:
        print('Install tikzplotlib if you want to save the figure as a tex file')
