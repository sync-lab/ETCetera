#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 14:44:09 2022

@author: ggleizer
"""

import ETCetera.Systems.linearetc as etc
import ETCetera.util.etcsim as etcsim
from ETCetera.Systems.sampleandhold import SampleAndHoldController
from ETCetera.Systems.linearsys import Plant, Controller
import ETCetera.Abstractions as ab

import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as la
import time

TTT = 50

def generate_plot_data(x0, trig, max_T):
    t0, t1 = 0, max_T
    t = np.arange(t0, t1, 0.01)

    out = etcsim.simulate_sample_and_hold_control(trig, t, x0)
    ks = np.array([x for x in range(len(t))])
    ks = ks[out['sample']]
    xs = out['xp']
    xs = xs[:,out['sample']]

    ttrig = t[ks]/trig.h

    return xs, out['t'], ttrig

if __name__ == '__main__':
    ''' System model '''
    '''Jet engine example from (Delimpaltadakis and Mazo Jr., 2020)

    x1\' = -x2 - 3/2*x1^2 - 1/2*x1^3
    x2\' = u

    u = 3x1 - 1/2*(x1^2 + 1)*(y + x1^2*y + x1*y^2),  y = (x1^2+x2)/(x1^2 + 1)
    '''

    h = 0.05
    sigma = 0.5  # Can be anywhere from 0 to 1
    kmax = 20

    nlplant = Plant()
    nlplant.nx = 2
    nlplant.nu = 1
    nlplant.ny = 2
    nlplant.states_as_output = True
    nlplant.measurement = lambda x, v: x

    nlcontroller = Controller()
    nlcontroller.ny = 2
    nlcontroller.nu = 1
    nlcontroller.h = h

    nletc = SampleAndHoldController(nlplant, nlcontroller)
    nletc._sampling_type = 'ETC'
    nletc.h = h
    nletc.kmax = kmax
    nletc.is_discrete_time = True
    nletc.triggering_function_uses_output = False
    nletc.triggering_is_time_varying = False
    nletc.sigma = sigma

    def fp(t, x, uhat, disturbance=None):
        return np.array([-x[1] - 1.5*x[0]**2 - 0.5*x[0]**3,
                         uhat[0]])

    def output(y, xc):
        x = y
        y = (x[0]**2 + x[1])/(x[0]**2 + 1)
        u = x[0] - 0.5*(x[0]**2 + 1)*(y + x[0]**2*y + x[0]*y**2)
        return np.array((u,))

    def trigger(dt, x, xhat, *args):
        # 0.9055 = sqrt(0.82) from the paper
        return la.norm(x-xhat) > nletc.sigma * 0.9055 * la.norm(x) \
            or np.round(dt/nletc.h) >= nletc.kmax

    nlcontroller.output = output
    nletc.fp = fp
    nletc.trigger = trigger


    '''
    Closed-loop linear model
    A = [0 -1; 0 0], B = [0; 1], K = [3, -0.5]
    '''

    # Plant
    Ap = np.array([[0, -1], [0, 0]]);
    Bp = np.array([0, 1]);

    # Controller
    K = np.array([[1, -0.5]])

    # Provided Lyapunov Matrix
    Pl = np.array([[1, .25], [.25, 1]])

    p = etc.LinearPlant(Ap, Bp, np.eye(2))
    Acl = p.A + p.B @ K

    controller = etc.LinearController(K, h)
    trig = etc.TabuadaPETC(p, controller, sigma=sigma * 0.9055,
                           h=h, kmax=kmax)

    ''' Compute SAIST '''
    t = time.time()
    traffic = ab.TrafficModelLinearPETC(trig,
                                        depth=100,
                                        etc_only=True,
                                        solver='z3',
                                        stop_if_mace_equivalent=True)
    traffic.create_abstraction()
    elt = time.time() - t
    print(f'Elapsed time for building the abstraction: {elt}')
    vlow = traffic.limavg
    vup, _ = traffic.automaton.minimum_average_upper_bound()
    print(f'Result: {vlow} <= SAIST <= {vup}')


    ''' Generate trajectories for Fig. 5 '''
    plt.figure(1)
    plt.figure(2)

    np.random.seed(1909)
    for x in np.random.rand(5,2):
        x = x - 0.5
        x0 = 2*x

        # Nonlinear simulation
        xs, tall, ttrig = generate_plot_data(x0, nletc, TTT)
        dt = np.diff(ttrig)
        nn = len(dt)
        runavg = np.cumsum(dt)/(np.arange(nn)+1)

        # Linearized model
        xss, talls, ttrigs = generate_plot_data(x0, trig, TTT)
        dts = np.diff(ttrigs)
        nns = len(dts)
        runavgs = np.cumsum(dts)/(np.arange(nns)+1)

        plt.figure(1)
        plt.plot(ttrig[:-1], runavg, 'g')
        plt.plot(ttrigs[:-1], runavgs, 'orange')
        plt.legend(('Nonlinear', 'Linear'))


    plt.figure(1)
    plt.plot([0, TTT/trig.h], [vlow, vlow], 'k:')
    plt.plot([0, TTT/trig.h], [vup, vup], 'k:')
    plt.xlabel('Normalized time')
    plt.ylabel('IST (running average)')

    plt.figure(2)
    linpred = []  # Predictions based on linear model from nonlinear state
    for x in xs.T:
        xpred, tpred, ttrigpred = generate_plot_data(x, trig, 2)
        linpred.append(ttrigpred[1])
    plt.stem(ttrig[:-1], dt, linefmt='green', markerfmt=('green', 'x'),
             basefmt='None')
    plt.stem(ttrig[:-1], linpred[:-1], linefmt='orange', markerfmt=('orange','o'),
                 basefmt='None')
    plt.legend(('Nonlinear', 'Linear'), loc='upper center')
    plt.xlabel('Normalized time')
    plt.ylabel('IST')
    plt.ylim((5,13.5))

    ax2 = plt.twinx()
    ax2.plot(ttrig, la.norm(xs, axis=0))
    ax2.legend(('State norm',))
    ax2.set_ylabel('State norm')


