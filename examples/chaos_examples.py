#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  7 15:06:29 2021

@author: ggleizer
"""


import ETCetera.Systems.linearetc as etc
import ETCetera.util.etcsim as etcsim

import matplotlib.pyplot as plt
import numpy as np

import control as ct
import scipy.linalg as la


if __name__ == '__main__':

    Ap = np.array([[0, 1], [-2, 3]])
    Bp = np.array([0, 1])
    p = etc.LinearPlant(Ap, Bp, np.eye(2))

    cases = [(0.2, np.array([[0,-5]]), 0),
             (0.32, np.array([[0,-6]]), 0),
             (0.5, np.array([[0,-6]]), 0),
             (0.6, np.array([[0,-6]]), 0),
             (0.32, np.array([[0,-6]]), 0.05)]

    for case, (sigma, K, hs) in enumerate(cases):
        h = 0.01
        controller = etc.LinearController(K, h)

        Ql = np.eye(2)
        Acl = p.A + p.B @ K
        print(la.eigvals(Acl))
        Pl = ct.lyap(Acl.T.copy(), Ql.copy())

        trig = etc.TabuadaPETC(p, controller, P=Pl, Q=Ql, sigma=sigma, h=h,
                               kmax=np.inf)
        if hs == 0:
            trig.is_discrete_time = False
            h = 1
        else:
            h = hs
            trig.h = hs
            trig.controller.h = hs

        # Find the states after many samples, and how they distribute
        ''' Map on -pi/2 to pi/2 '''
        if hs == 0:
            th = np.arange(-np.pi/2, np.pi/2, 0.01)
        else:  # More points to highlight discontinuity
            th = np.arange(-np.pi/2, np.pi/2, 0.001)
        f = np.nan * np.ones((len(th), 1000))
        tau = f.copy()
        for i,t in enumerate(th):
            x0 = np.array([np.cos(t), np.sin(t)])
            out = etcsim.simulate_sample_and_hold_control(trig, np.arange(0,5,h),
                                                          x0)
            xs = out['xphat'][:,out['sample']]
            f[i,:xs.shape[1]] = np.arctan(xs[1]/xs[0])

            dt = np.diff(out['t'][out['sample']])
            tau[i,:len(dt)] = dt

        # # Do a longer simulation from 1 initial condition for coweb plot
        if h > 0:
            t = 0
            x0 = np.array([np.cos(t), np.sin(t)])
            out = etcsim.simulate_sample_and_hold_control(trig, np.arange(0,16,h),
                                                          x0)
            xs = out['xphat'][:,out['sample']]
            fs = np.arctan(xs[1]/xs[0])
            cobweby = np.reshape(np.array([x for x in zip(fs,fs)]),len(fs)*2)
            cobwebx = cobweby[:-2].copy()
            cobweby = cobweby[1:-1]
        elif sigma > 0.3:
            # Numerical issues for small x (zero-crossing detection?)
            # Instead, use the map to do the cobweb
            NC = 200
            cobwebx = np.zeros((NC*2,))
            cobweby = np.zeros((NC*2,))
            fc = 0
            for i in range(NC):
                cobwebx[2*i] = fc
                cobweby[2*i] = fc
                fcn = np.interp(fc, th, f[:,1])
                cobwebx[2*i+1] = fc
                cobweby[2*i+1] = fcn
                fc = fcn

        #cobweby[0] = 0.

        # split discontinuous segments and plot
        f1 = f[:,1]
        if hs == 0:
            imin = np.argmin(f1)

            plt.figure()
            plt.plot(th[:imin], f1[:imin], 'b',
                     th[imin:], f1[imin:], 'b',
                     th[[0,-1]], th[[0,-1]], 'r--')
        else:  # Splitting is trickier
            plt.figure()
            dtau = np.diff(tau[:,0])
            ichange = list(np.nonzero(dtau)[0] + 1)
            Npoints = len(dtau)
            for i, j in zip([0] + ichange, ichange + [Npoints]):
                fi = f1[i:j]
                thi = th[i:j]
                imin = np.argmin(fi)
                if fi[imin] < -1 and fi[imin-1] > 1:  # Circle discontinuity
                    plt.plot(thi[:imin], fi[:imin], 'b',
                             thi[imin:], fi[imin:], 'b')
                else:
                    plt.plot(thi, fi, 'b')
            plt.plot(th[[0,-1]], th[[0,-1]], 'r--')

        if sigma > 0.3:  # Only for cases with fixed point
            plt.plot(cobwebx, cobweby, 'g')
        plt.grid()
        plt.xlabel(r'$\theta_i$')
        plt.ylabel(r'$\theta_{i+1}$')

        plt.savefig(f'ex1_K{K}_s{sigma}_h{hs}.png')

        plt.figure()
        plt.plot(th, tau[:,0])
        plt.grid()
        plt.xlabel(r'$\theta$')
        plt.ylabel(r'$\tau(\theta)$')

        plt.savefig(f'tau_ex1_K{K}_s{sigma}_h{hs}.png')

        # For reporting some values
        if case == 1:
            y = 0.3903
            pcy = etc.PeriodicLinearController(p, controller, y)
            print(la.eigvals(pcy.Phi))
            #array([ 0.75764648+0.j, -1.32843809+0.j])














