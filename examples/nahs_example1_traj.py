#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 14:44:09 2022

@author: ggleizer
"""

import ETCetera.Systems.linearetc as etc
import ETCetera.util.etcsim as etcsim
import numpy as np
import matplotlib.pyplot as plt

TTT = 20

def generate_plot_data(x0, trig, max_T):
    t0, t1 = 0, max_T
    t = np.arange(t0, t1, 0.01)

    out = etcsim.simulate_sample_and_hold_control(trig, t, x0)
    ks = np.array([x for x in range(len(t))])
    ks = ks[out['sample']]
    xs = out['xp']
    xs = xs[:,out['sample']]

    ttrig = t[ks]/trig.h

    return xs, out['t'], ttrig

if __name__ == '__main__':
    Ap = np.array([[0, 1], [-2, 3]])
    Bp = np.array([0, 1])
    p = etc.LinearPlant(Ap, Bp, np.eye(2))

    K = np.array([[0, -5]])
    h = 0.05

    controller = etc.LinearController(K, h)
    trig = etc.TabuadaPETC(p, controller, sigma=0.2, h=h, kmax=20)


    '''Example of the PETC'''
    plt.figure(1)
    # plt.figure(6)

    np.random.seed(1909)
    for x in np.random.rand(1,2):
        x = x - 0.5
        x0 = 2*x

        xs, tall, ttrig = generate_plot_data(x0, trig, TTT)

        dt = np.diff(ttrig)
        nn = len(dt)
        runavg = np.cumsum(dt)/(np.arange(nn)+1)


        plt.figure(1)
        plt.plot(ttrig[:-1], runavg, 'orange', label='Running average')
        plt.stem(ttrig[:-1], dt, linefmt='green', markerfmt=('green', 'x'),
                 basefmt='None', label='IST')


    plt.figure(1)
    plt.plot([0, TTT/trig.h], [2.74, 2.74], 'k:')
    plt.xlabel('Normalized time')
    plt.ylabel('IST')
    plt.legend(loc='upper right')
