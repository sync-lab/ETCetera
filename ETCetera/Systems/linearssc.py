#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 29 09:44:28 2022

@author: ggleizer
"""

import numpy as np
import logging

from ETCetera.Systems.linearetc import STC, LinearSampleAndHoldController, \
    ETCError
from ETCetera.Abstractions import TrafficModelLinearPETC




class SSCError(ETCError):
    pass


class SSC(STC, LinearSampleAndHoldController):
    """
    Self-Scheduled Control.

    Self-Scheduled Control (SSC) is a Self-Triggered Control that
    uses traffic abstractions to determine a near-optimal sampling
    strategy. See [1]_ for details. The basic SSC is state-dependent.

    Parameters
    ----------
    traffic : abstraction.TrafficModelPETC
        The traffic model for the underlying ETC
    most_permissive_strategy : dict (optional)
        A dictionary mapping a triple (region, Lyapunov level, time)
        into a dictionary of time-dependent choices. A time-dependent
        choice is a dictionary mapping an upper bound of elapsed time
        into a set of sampling times.
    optimal_strategy : dict
        A dictionary mapping either a region or a pair
        (region, Lyapunov level) into a discrete sampling instant
    """

    def __init__(self,
                 traffic_model: TrafficModelLinearPETC,
                 most_permissive_strategy: dict,
                 optimal_strategy: dict):

        self.plant = traffic_model.plant
        self.controller = traffic_model.controller
        assert traffic_model.trigger.is_discrete_time, \
            "SSC only implemented for discrete-time sampling"
        self.is_discrete_time = True

        # Extract relevant data from the traffic model
        self.h = traffic_model.trigger.h
        self.Q = traffic_model.Q
        self.predecessors = traffic_model.predecessors
        self.P = traffic_model.trigger.P
        try:
            self.V_range = (traffic_model.V_list[0], traffic_model.V_list[-1])
            self.num_levels = len(traffic_model.V_list)
            self.alpha = traffic_model.alpha
            self.is_homogeneous = False
        except AttributeError:
            self.is_homogeneous = True

        # Main data
        self.traffic_model = traffic_model
        self.most_permissive_strategy = most_permissive_strategy
        self.optimal_strategy = optimal_strategy

        # Get maximum sample count from strategy
        try:
            self.max_sample = max(c for _,_,c in most_permissive_strategy)
        except ValueError:
            self.max_sample = max(optimal_strategy.values())

    def region_of_state(self, x: np.array):
        """ Determines which region state x belongs

        Parameters
        ----------
        x: numpy.array
            Input state

        Returns
        -------
        int
            The region index

        """
        return self.traffic_model.region_of_state(x)

    def level_of_state(self, x: np.array):
        """ Determines the Lyapunov level where state x belongs.


        Parameters
        ----------
        x : np.array
            Input state

        Returns
        -------
        int
            The level index

        """
        return self.traffic_model.level_of_state(x)

    def sampling_time(self, x, xc=None, xs=np.array([0, 0]), **kwargs):
        """Compute the sampling time of the Self-Scheduled Controller.

        Parameters
        ----------
        x : numpy.array
            Plant state
        xc : numpy.array, ignored, optional
            Controller state (ignored). The default is None.
        xs : int, optional
            Scheduler state [c, t]'
        **kwargs :
            Additional parameters are discarded but accepted for
            compatibility

        Returns
        -------
        float > 0
            The sampling time.
        xs : numpy.array
            The updated scheduler states
        """
        if self.is_homogeneous:
            r = self.region_of_state(x)
            k = self.optimal_strategy[r]
            return self.h*k, xs

        r, lv = (self.region_of_state(x), self.level_of_state(x))
        if lv > self.num_levels:
            raise SSCError('level is out of bounds: %d', lv)
        #lv = max(0, min(98, lv))  # FIXME: JUST FOR DEBUG

        c = min(self.max_sample, int(xs[0]))
        t = int(xs[1])

        # Look up the available strategies
        try:
            actions = self.most_permissive_strategy[(r, lv, c)]
        except KeyError as e:
            # We are out of the domain of the scheduler, which
            # means it failed.
            raise SSCError(f'Out-of-domain: no strategy available for {e}')
        try:
            choices = next(v for x, v in actions.items() if t <= x)
        except StopIteration:
            # We are out of the domain of the scheduler, which
            # means it failed. This time, we are just too late.
            raise SSCError(f'Out-of-domain: no strategy available for'
                           f' {r, lv, c} at discrete time {t}.')
        # Try to see if the optimal strategy fits for this time
        opt = False
        try:  # First try the optimal strategy
            k_opt = self.optimal_strategy[(r, lv, c)]
            if k_opt in choices:
                k = k_opt
                opt = True
            else:
                logging.debug('Optimal strategy is not safe for this time')
        except KeyError:  # Optimal strategy not available
            logging.debug(f'No optimal strategy available at {r, lv, c}')
        if not opt:
            # TODO: possible heuristics: choose the best periodic or ETC's
            # One obvious heuristic:
            if c <= self.max_sample:
                k = max(choices)
            else:
                for k in choices:
                    break

        xs = xs + [1, k]
        return k * self.h, xs

    @staticmethod
    def fs(xs):
        """Compute the evolution of the scheduler states."""
        return xs + 1

    def evaluate_run(self, sim_out, target_level=None):
        """Generate metrics of a simulation.

        Compute metrics of a simulation with respect to the current Self
        -Scheduled Model, such as time to reach the innermost Lyapunov
        levelset and number of communications to do so.

        Parameters
        ----------
        sim_out : dict
            As returned by a ETC/STC simulation

        Returns
        -------
        time_to_reach : float
            Time it took to reach level zero
        sample_count : int
            Number of samples before reaching level zero
        """
        return super().evaluate_run(sim_out, target_level=self.V_range[0])

    def evaluate(self, N=1000, T=2, seed=None, initial_level=None,
                 target_level=None):
        """Evaluate the current SSC.

        Compute metrics for N randomly generated initial conditions.
        The initial conditions satisfy x(0)'Px(0) = V_max.

        Parameters
        ----------
        N : int, optional
            Number of simulations. The default is 1000.
        T : float, optional
            Max time of each simulation. The default is 2.
        seed : int, optional
            Seed for random number generation. The default is None,
            thus rng is not called.

        Returns
        -------
        time_to_reach_array : np.array(float)
            Time it took to reach level zero for each simulation.
        sample_count_array : np.array(int)
            Number of samples before reaching level zero for each
            simulation.
        """
        if initial_level is None or initial_level > self.V_range[1]:
            initial_level = self.V_range[1]
        if target_level is None or target_level < self.V_range[0]:
            target_level = self.V_range[0]
        return super().evaluate(N, T, seed, initial_level, target_level,
                                xs0=np.array([0, 0]))